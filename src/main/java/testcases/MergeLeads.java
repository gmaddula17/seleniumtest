package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethods;
import wdMethods.SeMethods;

public class MergeLeads extends ProjectSpecificMethods{
@Test(groups = "Regrsession" , dependsOnGroups = "Sanity")
public void Merge() throws InterruptedException
{
	
	WebElement crmsfa = locateElement("id","label" );
	click(crmsfa);
	Thread.sleep(3000);
	WebElement leads = locateElement("linktext", "Leads");
	click(leads);
WebElement merge= locateElement("linktext", "Merge Leads");
click(merge);
 WebElement icon=locateElement("xpath", "(//img[@alt='Lookup'])[1]");
 click(icon);
 switchToWindow(1);
 WebElement enterlid= locateElement("xpath", "//input[@name='id']");
type(enterlid, "10268");
WebElement tapfind= locateElement("xpath", "//a[@class='linktext']");

switchToWindow(0);
WebElement toLead= locateElement("xpath", "(//img[@alt='Lookup'])[2]");
 click(toLead);
 switchToWindow(1);
 WebElement leadid= locateElement("xpath","//input[@name='id']");
 click(leadid);
 type(leadid, "10268");
 WebElement tapfind1= locateElement("xpath", "//a[@class='linktext']");
 switchToWindow(0);
 WebElement merge1= locateElement("xpath","//a[@class='buttonDangerous']");
 click(merge1);
 Thread.sleep(3000);
 acceptAlert();
 WebElement findleads= locateElement("linktext", "Find Leads");
	click(findleads);
	WebElement find= locateElement("xpath", "//input[@name='id']");
	click(find);
	type(find, "10070");
	WebElement last= locateElement("xpath","(//button[@type='button'])[7]" );
	click(last);

}


	
}
