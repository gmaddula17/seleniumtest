package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethods;
import wdMethods.SeMethods;

public class LoginLogout extends ProjectSpecificMethods {
	
	@BeforeClass
	public void setData(){
		testCase = "LoginLogout";
		testCaseDesc = "Create lead with Mandate values";
		author = "Gayatri"; 
		category = "Regression";
		filename = "Excel";
	}
	
	
	
	//@Test(invocationTimeOut = 20000)
	//@Test(groups = "Smoke")
	@Test (dataProvider = "Getdata")
	public void login(String companyname, String firstname, String lastname ) throws InterruptedException{
		
		
		
		WebElement crm = locateElement("linktext","CRM/SFA");
		click(crm);

		WebElement create = locateElement("linktext", "Create Lead");
		click(create);
		WebElement company = locateElement("createLeadForm_companyName");
		type(company , companyname);
		WebElement first = locateElement("createLeadForm_firstName");
		type(first , firstname);
		WebElement last = locateElement("createLeadForm_lastName");
		type(last , lastname);
		WebElement source = locateElement("name", "dataSourceId");
		selectDropDownUsingText(source, "Conference");
				
		WebElement ownership = locateElement("name", "ownershipEnumId");
		selectDropDownUsingText(ownership , "Partnership");
		/* WebElement parent = driver.findElementByXPath("//img[@src ='/images/fieldlookup.gif']");
		 click(parent);
		 //switchToWindow(1);
	     WebElement link = driver.findElementByXPath("(//a[@class = 'linktext'])[1]");
	     click(link);
	    
	     //Thread.sleep(5000);
	     //switchToWindow(0);*/
	     WebElement submit = driver.findElementByClassName("smallSubmit");
	     click(submit);
	        
			
		
	}
	
	}
	










