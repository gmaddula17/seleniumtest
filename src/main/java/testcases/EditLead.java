package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethods;


public class EditLead extends ProjectSpecificMethods {	
	
	@Test
	public void EditLead() {
	   //    doLogin();
	     WebElement crm = locateElement("linktext","CRM/SFA");
	     click(crm);
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		WebElement findLeads = locateElement("linktext", "Find Leads");
		click(findLeads);
		WebElement leadid = locateElement("xpath", "//input[@name='id']");
		type(leadid, "10070");
		WebElement find = locateElement("xpath", "//button[text() ='Find Leads']");
		click(find);
		WebElement id = locateElement("xpath","//a[@href='/crmsfa/control/viewLead?partyId=10070']");
		click(id);
		verifyTitle("View Lead | opentaps CRM");
		WebElement edit = locateElement("linktext", "Edit");
		click(edit);
		WebElement compName = locateElement("id","updateLeadForm_companyName");
		compName.clear();
		type(compName, "Amazon");
		WebElement update = locateElement("classname", "smallSubmit");
		click(update);
		WebElement compNameupdate = locateElement("viewLead_companyName_sp");
		getText(compNameupdate);
		closeBrowser();
		

}

	
}


