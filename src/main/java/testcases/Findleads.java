
	package testcases;

	import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethods;
import wdMethods.SeMethods;

	public class Findleads extends ProjectSpecificMethods
	{
		
		@BeforeClass
		public void setData(){
			testCase = "LoginLogout";
			testCaseDesc = "Create lead with Mandate values";
			author = "Gayatri"; 
			category = "Regression";
		}

		//@Test(dependsOnMethods = "testcases.LoginLogout.login")
		@Test(groups = "Sanity" ,dependsOnGroups = "Smoke")
		public void find() throws InterruptedException{
			startApp("chrome","http://leaftaps.com/opentaps");
			
			WebElement crmsfa = locateElement("id","label" );
			click(crmsfa);
			Thread.sleep(3000);
				WebElement leads = locateElement("linktext", "Leads");
				click(leads);
				WebElement findleads= locateElement("linktext", "Find Leads");
				click(findleads);
				//Thread.sleep(3000);
				WebElement phone= locateElement("xpath", "//li[@class=' x-tab-strip-active']/following-sibling::li[1]");
				click(phone);
				WebElement numb= locateElement("xpath", "//input[@name='phoneNumber']");
				type(numb, "8870706476");
				WebElement find= locateElement("xpath", "(//button[@type='button'])[7]");
				click(find);
				WebElement rcell= locateElement("xpath", "(//a[@class='linktext'])[4]");
			String info= rcell.getText();
			System.out.println("1st resulting info :"+info);
				click(rcell);
				WebElement delete= locateElement("linktext", "Delete");
				click(delete);
				Thread.sleep(2000);
				WebElement findleads1= locateElement("linktext", "Find Leads");
				click(findleads1);
				
			WebElement passvalue=locateElement("xpath", "//input[@name='id']");
			type(passvalue, info);
			WebElement finalclick= locateElement("xpath","(//table[@cellpadding='0'])[8]");
			click(finalclick);
				driver.close();
				
	}
	}



