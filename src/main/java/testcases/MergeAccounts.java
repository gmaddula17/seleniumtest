package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethods;


public class MergeAccounts extends ProjectSpecificMethods{
	
@Test

public void login() throws InterruptedException{
	
	
		WebElement crm = locateElement("linktext","CRM/SFA");
		click(crm);
		WebElement account= locateElement("xpath", "//a[@href='/crmsfa/control/accountsMain']");
		click(account);
		WebElement mergeacc = locateElement("xpath","//a[@href ='/crmsfa/control/mergeAccountsForm']");
		click(mergeacc);
		WebElement fromacc = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[1]");
		click(fromacc);
		switchToWindow(1);
		Thread.sleep(3000);
		WebElement name = locateElement("name","accountName");
		type(name, "Barath12");
		WebElement find = locateElement("id","ext-gen110");
		click(find);
		WebElement add = locateElement("classname","linktext");
		Noclick(add);
		
		switchToWindow(0);
		WebElement toacc = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
		click(toacc);
		switchToWindow(1);
		Thread.sleep(3000);
		WebElement accid = locateElement("xpath","//input[@name='id']");
		type(accid, "10544");
		WebElement find1 = locateElement("id","ext-gen110");
		click(find1);
		WebElement add1 = locateElement("classname","linktext");
		Noclick(add1);
		switchToWindow(0);
		
		WebElement merge = locateElement("classname","buttonDangerous");
		Noclick(merge);
		
		Thread.sleep(3000);
		acceptAlert();
		

		WebElement findacc = locateElement("xpath","//input[@name='id']]");
		type(findacc, "10574");
		WebElement find3 = locateElement("//button[@id='ext-gen110']");
		click(find3);
		WebElement error = locateElement("linkText","No records to display");
				
		
}



}
