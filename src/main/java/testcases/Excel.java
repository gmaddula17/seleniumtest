package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	public static Object[][] exceldatasheet(String  filename) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheet("CreateLead");
		
		 //XSSFRow row = sheet.getRow(1);
		// XSSFCell cell = row.getCell(1);
		
		// String stringCellValue = cell.getStringCellValue();
		// System.out.println(stringCellValue); 
       int rowCount = sheet.getLastRowNum();
         System.out.println("Row count is:"+ rowCount);
         
         int columnCount = sheet.getRow(0).getLastCellNum();
         System.out.println("Column count is:" + columnCount);
         
         Object[][] datagp = new Object[rowCount][columnCount];
        
         for(int i=1; i<=rowCount; i++) {
        	 XSSFRow row = sheet.getRow(i);
        	 
        	 for(int j=0; j<columnCount; j++) {
        		 XSSFCell cell = row.getCell(j);
        		 
        		 
        		 String stringCellValue = cell.getStringCellValue();
        		 
        		 datagp[i-1][j] = stringCellValue;
        		 System.out.println(stringCellValue); 
        	 }
         }
		return datagp; 
        
		
	}

}
