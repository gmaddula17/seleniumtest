package Reports;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {

	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public static ExtentTest test;
	public String testCase, testCaseDesc, author, category, filename;
	
	
	public  void startResult(){
		html = new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}


	public void startTestCase() {
		test = extent.createTest(testCase , testCaseDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}


	public void logStep(String desc, String status){
		if(status.equalsIgnoreCase("PASS")) {
			test.pass(desc);
		}else if(status.equalsIgnoreCase("FAIL")) {
			test.fail(desc);
		}else if(status.equalsIgnoreCase("WARN")){
			test.fail(desc);
		}
	}
	
	
	
	public void name() {
		extent.flush();
	}

}
