package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import testcases.Excel;

public class ProjectSpecificMethods extends SeMethods{
	
	/*@BeforeSuite
	public void beforeSuite() {
		startResult();
	}
	
	@BeforeClass
	public void testCase() {
		startTestCase();
	}*/
	
	
	
	@DataProvider (name = "Getdata")
	public  Object[][] getdata() throws IOException {
		/*String[][] data = new String[2][3];
		data[0][0] = "TL";
		data[0][1] = "Gayatri";
		data[0][2] = "Maddula";
		
		data[1][0] = "Sri";
		data[1][1] = "Sai";
		data[1][2] = "pri";*/
		
		return Excel.exceldatasheet(filename);
	}
		
    @Parameters({"browser" ,"url", "uname", "upwd"})
	
	@BeforeMethod
	public  void doLogin( String browsername , String URL, String Username, String Password) {
		
		startApp(browsername, URL);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, Username);
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, Password);
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);
		
	}
	
	/*@AfterSuite
	public void stop() {
		name();
	}*/
	
@AfterMethod
	public void closeApp() {
	   closeAllBrowsers();
	}


}
