package week3.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Browser {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		
     ChromeDriver driver = new ChromeDriver();
     driver.manage().window().maximize();
     driver.get("http://leaftaps.com/opentaps/");
     driver.findElementById("username").sendKeys("DemoSalesManager");
     driver.findElementById("password").sendKeys("crmsfa");
     driver.findElementByClassName("decorativeSubmit").click();
     
     driver.findElementByLinkText("CRM/SFA").click();
     driver.findElementByLinkText("Create Lead").click();
     driver.findElementById("createLeadForm_companyName").sendKeys("SaiAcademy");
     driver.findElementById("createLeadForm_firstName").sendKeys("Gayatri");
     driver.findElementById("createLeadForm_lastName").sendKeys("Maddula");
     driver.findElementByXPath("//img[@src ='/images/fieldlookup.gif']").click();
     
     Set<String> allWindows = driver.getWindowHandles();
     List<String> first = new ArrayList();
     first.addAll(allWindows);
     driver.switchTo().window(first.get(1));
     driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
     driver.findElementById("username").sendKeys("DemoSalesManager");
     driver.findElementById("password").sendKeys("crmsfa");
     driver.findElementByXPath("(//a[@class ='loginButton'])[1]").click();
     
     
     
   //  WebElement source = driver.findElementById("createLeadForm_dataSourceId");
   //  Select sc = new Select(source);
     //sc.selectByVisibleText("Public Relations");
     
    // WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
     //Select ma = new Select(marketing);
     //ma.selectByValue("CATRQ_CARNDRIVER");
     
     //WebElement industry =  driver.findElementById("createLeadForm_industryEnumId");
     //Select in = new Select(industry);
     //in.selectByIndex(14);
     
     //driver.findElementByClassName("smallSubmit").click();
     //driver.close();
	}

}
