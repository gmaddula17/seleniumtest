package week3.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import cucumber.api.java.eo.Se;

public class FlipKart {

	private static final Set<String> String = null;
	
	@Test
	public void Flip() throws InterruptedException {

	//public static void main(String[] args) throws InterruptedException {
		//setting chrome driver
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		//launching flipkart site using chrome
		driver.get("https://www.flipkart.com/");
		
		//closing popup
		driver.findElementByXPath("//button[text() = '✕']").click();
		
		//clicking electronics
		//driver.findElementByXPath("//span[text()= 'Electronics']").click();
		
		//placing moseover
		WebElement electronics = driver.findElementByXPath("//span[text()= 'Electronics']");
		Actions builder = new Actions(driver);
		builder.moveToElement(electronics).perform();
		 Thread.sleep(5000);
		 
		driver.findElementByLinkText("Mi").click();
         Thread.sleep(5000);
		
		//get title
		String gettitle = driver.getTitle();
		if(gettitle.contains("Mi Mobile Phone")) {
			System.out.println(gettitle);
		}else {
			System.out.println("Title getting error");
		}
		
		//click on new
		driver.findElementByXPath("//div[text()='Newest First']").click();
		
		//identifying name,prices xpath and printing
		List<WebElement> names = driver.findElementsByXPath("//div[@class='_3wU53n']");		
		for (int i = 0; i < names.size(); i++) {
           System.out.println(names.get(i).getText());			
		}
		
		List<WebElement> prices = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		for (int i = 0; i < prices.size(); i++) {
			System.out.println(prices.get(i).getText());
		}

		//product click
		driver.findElementByXPath("(//img[@class='_1Nyybr  _30XEf0'])[1]").click();
		
		//window handles
		Set<String> allwindows = driver.getWindowHandles();
		List<String> list = new ArrayList<>();
		list.addAll(allwindows);
		driver.switchTo().window(list.get(1));
		
		Thread.sleep(3000);
		
		//getting title
		if(gettitle.contains("Redmi 6 Pro(Black, 32GB)")) {
			System.out.println(gettitle);
		}else {
			System.out.println("Title getting error");
		}
		
		//rating printing
		String Str1= driver.findElementByXPath("//div[@class='_3ors59']").getText();
		System.out.println(Str1);
		
		driver.quit();
	}

}
