package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		
	     ChromeDriver driver = new ChromeDriver();
	     driver.manage().window().maximize();
	     driver.get("https://www.irctc.co.in/nget/user-registration");
	     driver.findElementById("userName").sendKeys("Gayatri");
	     driver.findElementById("usrPwd").sendKeys("Aparna123");
	     driver.findElementById("cnfUsrPwd").sendKeys("Aparna123");
	     
	     WebElement security = driver.findElementByLinkText("Select Security Question");
	     Select SC = new Select(security);
	     SC.equals("What is your favorite past-time?");
	     
	     driver.findElementByPartialLinkText("secAns").sendKeys("Chennai2017");
	     
	     WebElement language = driver.findElementByLinkText("Select Preferred Language");
	     Select la = new Select(language);
	     la.equals("English");
	     
	     driver.findElementById("firstName").sendKeys("Maddula");
	     driver.findElementById("middleName").sendKeys("PVSAS");
	     driver.findElementById("lastname").sendKeys("Gayatri");
	     
	     driver.findElementByClassName("col-md-3 col-sm-4 col-xs-4").click();
	     
	     WebElement month = driver.findElementByClassName("ui-datepicker-month ng-tns-c12-7 ng-star-inserted");
	     Select mon = new Select(month);
	     mon.selectByVisibleText("August");
	     
	     WebElement year = driver.findElementByClassName("ui-datepicker-month ng-tns-c12-7 ng-star-inserted");
	     Select yea = new Select(year);
	     yea.selectByVisibleText("1994");
	     
	     WebElement occupation = driver.findElementByLinkText("Select Occupation");
	     Select occu = new Select(occupation);
	     occu.equals("PRIVATE");
	     
	     WebElement country = driver.findElementByClassName("form-control ng-valid ng-touched ng-dirty");
	     Select con = new Select(country);
	     con.selectByVisibleText("India");
	     
        driver.findElementById("email").sendKeys("gayathrimaddula17@gmail.com");
        driver.findElementById("mobile").sendKeys("9494826324"); 
        
       WebElement nation = driver.findElementByName("nationality");
       Select nat = new Select(nation);
	     nat.selectByVisibleText("India"); 
	     
	     driver.findElementByName("resAddress1").sendKeys("10th block");
	     driver.findElementByName("resAddress3").sendKeys("Ramapuram");
	     driver.findElementByName("resPinCode").sendKeys("600018");
	     driver.findElementByName("resState").sendKeys("Tamilnadu");
	     driver.findElementByPartialLinkText("resCity").sendKeys("Chennai");
	     
	     WebElement post = driver.findElementByPartialLinkText("resPostOff");
	     Select pos = new Select(post);
	     pos.selectByVisibleText("Chennai G.P.O.");
	     
	     driver.findElementByName("resPinCode").sendKeys("9494826324");
	     
	}

}
