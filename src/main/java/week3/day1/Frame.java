package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class Frame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		
	     ChromeDriver driver = new ChromeDriver();
	     driver.manage().window().maximize();
	     driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	     driver.switchTo().frame("iframeResult");
	    driver.findElementByXPath("//button[text() ='Try it']").click();
	    driver.switchTo().alert().sendKeys("Gayatri");
	    driver.switchTo().alert().accept();
	    String text = driver.findElementById("demo").getText();
	    System.out.println(text);
	    
	    if (text.equals("Hello Gayatri! How are you today?")){
	    	System.out.println("True");
	    }else
	    {
	    	System.out.println("False");
	    }
	     
	}

}
