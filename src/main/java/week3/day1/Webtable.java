package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Webtable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		
	     ChromeDriver driver = new ChromeDriver();
	     driver.manage().window().maximize();
	     driver.get("https://erail.in");
	     
	     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	     driver.findElementById("txtStationFrom").clear();
	     driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
	     driver.findElementById("txtStationTo").clear();
	     driver.findElementById("txtStationTo").sendKeys("RJY", Keys.TAB);
	     
	     WebElement check = driver.findElementById("chkSelectDateOnly");
	     if(check.isSelected()) {
	    	 check.click();
	     }
	    
	     //table level
	    WebElement table = driver.findElementByXPath("//table[@class = 'DataTable TrainList']");
	
	    //rowlevel
	    List <WebElement> row = table.findElements(By.tagName("tr"));
	    System.out.println(row.size());
	    for(int i=0; i<row.size(); i++) {
	     WebElement Row = row.get(i);	
	    //columnlevel
	    List<WebElement> column = table.findElements(By.tagName("td"));
	    System.out.println(column.size());
	    String SecondColumn = column.get(1).getText();
	    System.out.println(SecondColumn);
	    
	    }
	}
	
	

}
