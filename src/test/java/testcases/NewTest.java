package testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class NewTest {
 
  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("Wakeup");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("Brush");
  }

  @BeforeClass
  public void beforeClass() {
	  System.out.println("Formals");
  }
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("Get in ODC");
  }
  @Test
  public void f() {
	  System.out.println("Function");
  }
 
  @AfterMethod
  public void afterMethod() {
	  System.out.println("Get out");
  }
  @AfterClass
  public void afterClass() {
	  System.out.println("Casuals");
  }


  @AfterTest
  public void afterTest() {
	  System.out.println("Bed Brush");
  }

 
  @AfterSuite
  public void afterSuite() {
	  System.out.println("Sleep");
  }

}
